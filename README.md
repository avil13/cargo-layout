- [src](https://projects.invisionapp.com/share/CBF57TNUT#/screens/276487060)
- [zpl](https://app.zeplin.io/project/5a795d654e212eeb4e97f52f)

### Доработать:
|   Название    | файл |
|---|---|
| Регистрация 1  | fmt_reg_personal_data.xml|
| Регистрация 2  | fmt_reg_legal_data.xml   |
| Регистрация 3  | fmt_reg_bank_details.xml |
| Заказы Новый пользователь | fmt_orders.xml -> item_order.xml |
| Объявление Без Ставки     | ac_order_details.xml |

### Новое:
|   Название    | файл |
|---|---|
| Сделать ставку | fmt_auction_application.xml |
| Поиск |  item_search_paragraph.xml |
| Избранное | item_favorit.xml |
| Стоянка Инфо | fmt_parking_info.xml |
| Новое Объявление 1 | fmt_new_ad.xml |
| Предложения | item_proposal.xml |